
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SynchronizedKeyboardListener extends StatefulWidget {
	const SynchronizedKeyboardListener({required this.keyEvents, required this.child, Key? key}) : super(key: key);

	final Map<LogicalKeyboardKey, Function()> keyEvents;
	final Widget child;

	@override
	State<SynchronizedKeyboardListener> createState() => SynchronizedKeyboardListenerState();
}

class SynchronizedKeyboardListenerState extends State<SynchronizedKeyboardListener> {

	FocusNode node = FocusNode();
	SynchronizedKeyboardListenerState? parent;
	List<SynchronizedKeyboardListenerState> children = <SynchronizedKeyboardListenerState>[];

	@override
	void dispose() {
		if (parent != null) {
			parent!.removeChild(this);
		}
		node.dispose();
		super.dispose();
	}

	/// Works similarly to the .of() functions except we don't check ourself
	/// as that would cause the tree of [SynchronizedKeyboardListenerState]'s
	/// to be circular
	SynchronizedKeyboardListenerState? getAncestorSKL(BuildContext context) {
		return context.findAncestorStateOfType<SynchronizedKeyboardListenerState>();
	}

	@override
	void initState() {
		parent = getAncestorSKL(context);
		if (parent != null) {
			parent!.addChild(this);
		}
		super.initState();
	}

	void addChild(SynchronizedKeyboardListenerState child) {
		children.add(child);
	}

	void removeChild(SynchronizedKeyboardListenerState child) {
		children.remove(child);
	}

	/// Bubbles down the keyboard event. Returns true if anyone below us
	/// managed to handle it.
	bool bubbleDown(RawKeyEvent event) {
		for (int i = 0; i < children.length; i++) {
			if (children[i].bubbleDown(event)) {
				return true;
			}
		}
		if (node.hasFocus && widget.keyEvents.containsKey(event.logicalKey) && event.isKeyPressed(event.logicalKey)) {
			widget.keyEvents[event.logicalKey]!();
			return true;
		}
		return false;
	}

	@override
	Widget build(BuildContext context) {
		return RawKeyboardListener(
			autofocus: true,
			focusNode: node,
			onKey: (RawKeyEvent key) {
				if (parent != null) {
					return;
				}
				bubbleDown(key);
			},
			child: widget.child,
		);
	}
}
