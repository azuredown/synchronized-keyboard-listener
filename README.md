# synchronized_keyboard_listener

A keyboard listener with the ability to block keyboard events from bubbling up. This means if you have multiple `SynchronizedKeyboardListener`'s only one will be able to act on any given keyboard event: the one lowest down in the tree that is listening for that particular key.

How it works is upon creation it looks for parent `SynchronizedKeyboardListener`'s and forms a linked list of them. This allows them to synchronize their behaviour.

# Usage

The widget is self explanatory.

```dart
SynchronizedKeyboardListener(
  keyEvents: <LogicalKeyboardKey, Function()> {
    LogicalKeyboardKey.escape : () { Navigator.of(context).pop(); },
    LogicalKeyboardKey.enter : () { Navigator.of(context).pop(); },
  },
  child: ...
)
```

The key events argument tells us which keys to listen for and what to do if they are triggered.

To block keyboard events from bubbling up simply use an empty function like so:

```dart
SynchronizedKeyboardListener(
  keyEvents: <LogicalKeyboardKey, Function()> {
    LogicalKeyboardKey.enter : () {},
  },
  child: ...
)
```

This will block all enter key events from being bubbled up.
